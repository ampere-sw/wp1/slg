To execute ROS SLG, below is the command:


java -jar ros2_slg.jar -p <path of the properties file> -w <path of the working director>

--------------------------------------------------------------------------------------------------
Example:

java -jar ros2_slg.jar -p input.properties -w C:\temp\transformationFrameworkRepo\jar\latest\ros2


----------------------------------------------------------------------------------------------------

Example content of the properties file ( Note: here path of the files or directories can be relative to the working directory location, which is supplied as a command line parameter to the tool):

input_models_folder=./input_ODAS
configurationFile=./input/ros.config
output_folder=./output
log_file=./output/transformation.txt
m2m_output_folder=./output/m2m_output_models
m2t_output_folder=./output/m2t_output_text_files
m2tTransformers=ROS_SLG

-------------------------------------------------------------------------------------------------------




Transformation configured via Properties file:
[-p, --properties <file>] [-w, --workingDirectory <dir>]

Transformation configured via parameter:
[-o, --output <dir>] [-m, --m2m <keys>] [-t, --m2t <keys>] [-A<key-value>] <directory>

Options:
        -p, --properties        The Properties file that should be used to configure the transformation.
                        If the properties option is not set, the input needs to be provided as parameter.

        -w, --workingDirectory  The working directory. Used to resolve file parameters in the Properties file in a relative manner. [optional].

        -o, --output    The directory in which the output should be generated. Only interpreted if --properties is not set. [optional].
                        If not provided the output directory defaults to <input_directory>/result.

        -m, --m2m       Comma separated list of m2m transformation keys [optional].
                        If not provided and no m2t transformation keys are set, all available m2m transformations will be executed.

        -t, --m2t       Comma separated list of m2t transformation keys [optional].
                        If not provided and no m2m transformation keys are set, all available m2t transformations will be executed.

        -A              Option to provide additional transformation specific properties. [optional].
                        Need to be provided in the key=value format.

        -h, --help, /?  Show this help.

Parameter:
        directory       The directory that contains model files to transform

Available M2M transformations:

Available M2T transformations:
ROS_SLG
